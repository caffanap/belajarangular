export interface ProfileInterface{
    id: number;
    nama: string;
    alamat: string;
    gender: string;
    gaji: number;
}