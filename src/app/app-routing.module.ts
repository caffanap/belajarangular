import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ListdataComponent } from './listdata/listdata.component';
import { ProfiledataComponent } from './profiledata/profiledata.component';


const routes: Routes = [
  {
    path: '', component: ProfiledataComponent
  },
  {
    path: 'list', component: ListdataComponent
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
