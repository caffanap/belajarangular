import { Component, OnInit } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { ProfileInterface } from '../interface/profile.interface';
import { Router } from '@angular/router';

@Component({
  selector: 'app-profiledata',
  templateUrl: './profiledata.component.html',
  styleUrls: ['./profiledata.component.less']
})
export class ProfiledataComponent implements OnInit {

  profile: ProfileInterface;

  id: number;
  nama: string;
  alamat: string;
  gender: string;
  gaji: number;
  
  constructor(private routess: Router,private ambilsokointenet: HttpClient) { }
  
  ngOnInit() {
  }

  getData(){
    this.ambilsokointenet.get<ProfileInterface>('backend/rest-api/tampil_data.php').subscribe(val => {
      this.profile = val;
    });
  }

  tambahData(){
    
    let data = "id="+this.id+"&nama="+this.nama+"&alamat="+this.alamat+"&gender="+this.gender+"&gaji="+this.gaji;
    this.ambilsokointenet.post("backend/rest-api/tambah_data.php", data, {
      headers: new HttpHeaders().set('Content-Type', 'x-www-form-urlencoded')
    }).subscribe(val => {
      console.log(val);
      
    });
  }

  pindah(){
    this.routess.navigate(['/list']);
  }

  deleteData(id){
    this.ambilsokointenet.get("backend/rest-api/hapus_data.php?id="+id).subscribe(val => {
      console.log(val);
      this.getData();
    })
  }

}
